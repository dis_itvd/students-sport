'use strict';

$(function() {

    // Variables

    let isVideos            = $('div').is('.videos__row');
    let isResultSlider      = $('div').is('.result__slider');
    let isGallery           = $('div').is('.gallery');
    let isVideoGroup        = $('div').is('.video-group__slider');
    let isMinishop          = $('div').is('.minishop');
    let isChronicleSlider   = $('div').is('.chronicle__events--slider');
    let isVideoThumbSlider  = $('div').is('.videothumb__slider');
    let isStorySlider       = $('div').is('.stories__slider');
    let isTopSlider         = $('div').is('.top-block__slider');
    let isPartners          = $('div').is('.partners__slider'); 
    let isTeamSlider        = $('div').is('.team__slider'); 
    let issuccessSlider     = $('div').is('.success__slider'); 
    let isPlayerContent     = $('div').is('.player-content__slider'); 
    let isProfileStat       = $('div').is('.profile-stat__slider'); 
    let isPlayersSlider     = $('div').is('.players__slider'); 
    let isPostGallery       = $('div').is('.post-gallery__slider'); 
    let isInformers         = $('div').is('.informers-slider'); 
    let isInformersTwo      = $('div').is('.informers-slider-two'); 
    let isSportType         = $('div').is('.sport-type-slider'); 
    let isMobileGallery     = $('div').is('.mobile-gallery-slider'); 

    
    
    // SVG IE11 support
    svg4everybody();

    // Mobile Nav
    $('.nav-toggle').on('click', function(e){
        e.preventDefault();
        $('.nav-mobile').toggleClass('open');
    });

   

    // Tabs

    $('.tab__nav--item').on('click', function(e){
        e.preventDefault();
        let tab = $(this).closest('.tab');
        let tabNav = $(this).closest('.tab__nav');
        let tabItem = '.' + $(this).attr('data-tab');

        tabNav.find('.tab__nav--item').removeClass('active');
        $(this).addClass('active');

        tab.find('.tab__item').removeClass('active');
        tab.find(tabItem).addClass('active');
    });

    $('.tabs__nav-item').on('click', function(e){
        e.preventDefault();
        let tabs = $(this).closest('.tabs');
        let tabsItem = '.' + $(this).attr('data-tab');

        tabs.find('.tabs__nav-item').removeClass('active');
        $(this).addClass('active');

        tabs.find('.tabs__item').removeClass('active');
        tabs.find(tabsItem).addClass('active');
    });

 
    $('.success__nav--item').on('click', function(e){
        e.preventDefault();

        let slide = $(this).attr('data-slide');
        success.slideTo(slide, 600);
    });

    // Videos block nav

    
    // videos Slider
    if (isVideos) {
            
        let videoListSlider = new Swiper('.active .video-list__slider',{ 
            loop: true,
            direction: 'vertical',
            slidesPerView: 'auto',
            spaceBetween: 14,
            loop: true,
            freeMode: true,
            loopedSlides: 5, 
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });

        let videoMainSlider = new Swiper('.active .videos__slider',{ 
            loop: true,
            slidesPerView: 1,
            spaceBetween: 20,
            loop: true,
            loopedSlides: 5, 
            thumbs: {
                swiper: videoListSlider,
              },
        });

        $('.videos__header--nav-item').on('click', function(e){
            e.preventDefault();
    
            let tabs = $(this).closest('.videos');
            let tabsItem = '.tab' + $(this).attr('data-tab');
            let tabSlider = '.video-slider-' + $(this).attr('data-tab');
    
            $('.videos__header--nav-item').removeClass('active');
            $(this).addClass('active');

            videoMainSlider.destroy();
            videoListSlider.destroy();
    
            tabs.find('.videos__block').removeClass('active');
            tabs.find(tabsItem).addClass('active');
            
            videoListSlider = new Swiper('.active .video-list__slider',{ 
                loop: false,
                direction: 'vertical',
                slidesPerView: 'auto',
                spaceBetween: 14,
            })
    
            videoMainSlider = new Swiper('.active .videos__slider',{ 
                loop: false,
                slidesPerView: 1,
                spaceBetween: 20,
                loop: true,
                loopedSlides: 5, 
                thumbs: {
                    swiper: videoListSlider,
                  },
            });
        });
       
    }

    $('.university__main--toggle').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('open');
        $('.university__main--text-hide').toggleClass('open');
    });
    
    $('.university__info--toggle').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('open');
        $('.university__info--right').slideToggle('fast');
    });

    $( window ).resize(function() {
        if ($(window).width() > 1023) {
            $('.university__info--right').removeAttr('style');
        }
    });

    $('.datepicker-main').datepicker();

    $('.news-events__nav a').on('click', function(e){
        e.preventDefault();
        $('.news-events__nav a').removeClass('active');
        $(this).addClass('active');
    });
    
    $('.tribune__header--nav-item').on('click', function(e){
        e.preventDefault();
        $('.tribune__header--nav-item').removeClass('active');
        $(this).addClass('active');
    });

    $('.select').selectric({
        maxHeight: 200,
        disableOnMobile: false,
    });

    // Product Slider
    if (isMinishop) {
        let minishop = new Swiper('.minishop__slider',{ 
            loop: false,
            slidesPerView: 'auto',
            spaceBetween: 15,
            navigation: {
                nextEl: '.minishop__nav--next',
                prevEl: '.minishop__nav--prev',
            },
            scrollbar: {
                el: '.minishop__scrollbar',
            },
            breakpoints: {
                1170: {
                  slidesPerView: 4,
                  spaceBetween: 37,
                },
              }
        });
    
        minishop.on('slideChange', function () {
            let sld = minishop.realIndex + 1;
            $('.minishop__counter span').text(sld);
        });
    }

    if (isVideoGroup) {
        let videoGroup = new Swiper('.video-group__slider',{ 
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.video-group__scrollbar',
            }
        })
        
        videoGroup.on('slideChange', function () {
            let sld = videoGroup.realIndex + 1;
            $('.video-group__counter--current').text(sld);
        });    
    }

    // Result Slider
    if (isResultSlider) {

        let resultSlider = new Swiper('.result__slider',{ 
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.result__nav--scrollbar',
            },
            breakpoints: {
                1024: {
                  slidesPerView: 3,
                  spaceBetween: 30,
                }
            }
        })

        resultSlider.on('slideChange', function () {
            let sld = resultSlider.realIndex + 1;
            $('.result__nav--counter-value').text(sld);
        });
    
    }
    
    // videoThumb Slider
    if (isVideoThumbSlider) {

        let videoThumb = new Swiper('.videothumb__slider',{ 
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.videothumb__nav--scrollbar',
            },
            breakpoints: {
                768: {
                  slidesPerView: 3,
                  spaceBetween: 30,
                }
              }
        })

        videoThumb.on('slideChange', function () {
            let sld = videoThumb.realIndex + 1;
            $('.videothumb__nav--counter-value').text(sld);
        });
    
    }
        
    // Gallery Slider
    if (isGallery) {
        let gallery = new Swiper('.gallery__slider',{ 
            loop: true,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.gallery__scrollbar',
            }
        })
    
        gallery.on('slideChange', function () {
            let sld = gallery.realIndex + 1;
            $('.gallery__counter--current').text(sld);
        })
    }

    // chronicle Slider
    if (isChronicleSlider) {

        let chronicleSlider = new Swiper('.chronicle__events--slider',{ 
            loop: false,
            init: false,
            slidesPerView: 'auto',
            spaceBetween: 30,
            scrollbar: {
                el: '.chronicle__events--scrollbar',
            },
            on: {
                slideChange: function () {            
                    let sld = chronicleSlider.realIndex + 1;
                    $('.chronicle__events--counter span').text(sld);
                },
            }
        })

        if ($(window).width() < 970) {
            chronicleSlider.init();
        }
    }


    // Story Slider
    if (isStorySlider) {
        let storySlider = new Swiper('.stories__slider',{ 
            loop: false,
            slidesPerView: 1,
            spaceBetween: 20,
            loopAdditionalSlides: 0
        })
    
        storySlider.on('slideChange', function () {
            let slide = '.story-' + storySlider.realIndex;
            $('.stories__nav--item').removeClass('active');
            $(slide).addClass('active');
          });
        
        $('.stories__nav--item').on('click', function(e){
            e.preventDefault();
    
            let slide = $(this).attr('data-slide');
            storySlider.slideTo(slide, 600);
            $('.stories__nav--item').removeClass('active');
            $(this).addClass('active');
        });
    }
    // top Slider
    if (isTopSlider) {
        let topSlider = new Swiper('.top-block__slider', {
        loop: true,
        spaceBetween: 16,
        pagination: {
            el: '.top-block__pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
        },
    });

    }

     // Partners Slider
    if (isPartners) {
        let partners = new Swiper('.partners__slider',{ 
            loop: false,
            slidesPerView: 'auto',
            spaceBetween: 0,
            scrollbar: {
                el: '.partners__scrollbar',
            },
            breakpoints: {
                1170: {
                  slidesPerView: 5
                },
              }
        });
    }

    if (isTeamSlider) {
        
        let teamSlider = new Swiper('.team__slider',{ 
            loop: true,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.team__nav--scrollbar',
            },
            breakpoints: {
                768: {
                  slidesPerView: 2,
                  spaceBetween: 30,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                }
            }
        })
    }


    if (issuccessSlider) {
    
        let success = new Swiper('.success__slider',{ 
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
        })
    }
     // Partners Slider
     if (isPlayerContent) {

        let PlayerContent = new Swiper('.player-content__slider',{ 
            loop: true,
            slidesPerView: 2,
            spaceBetween: 30,
            scrollbar: {
                el: '.player-content__scrollbar',
            },
            breakpoints: {
                567: {
                  slidesPerView: 3
                },
              }
        });
    
        PlayerContent.on('slideChange', function () {
            let sld = PlayerContent.realIndex + 1;
            $('.player-content__counter--current').text(sld);
        })
    }

    if (isProfileStat) {

        let ProfileStat = new Swiper('.profile-stat__slider',{ 
            loop: false,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.profile-stat__scrollbar',
            }
        });
    
        ProfileStat.on('slideChange', function () {
            let sld = ProfileStat.realIndex + 1;
            $('.profile-stat__counter--current').text(sld);
        })
    }

    if (isPlayersSlider)  {
        
        let PlayersStat = new Swiper('.players__slider',{ 
            loop: false,
            slidesPerView: 1,
            spaceBetween: 24,
            scrollbar: {
                el: '.players__scrollbar',
            },
            breakpoints: {
                1024: {
                    spaceBetween: 35,
                },
            }
        });
    
        PlayersStat.on('slideChange', function () {
            let sld = PlayersStat.realIndex + 1;
            $('.players__counter--current').text(sld);
        })
    }

    if (isPostGallery) {
        let PostGallery = new Swiper('.post-gallery__slider',{ 
            loop: true,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.post-gallery__scrollbar',
            }
        })
    
        PostGallery.on('slideChange', function () {
            let sld = PostGallery.realIndex + 1;
            $('.post-gallery__counter-value').text(sld);
        })
    }

    $('.result__switch--item').on('click', function(e){
        e.preventDefault();
        $('.result__switch--item').removeClass('active');
        $(this).addClass('active');
    });
    
    $('.faq__question').on('click', function(e){
        e.preventDefault();
        let faq = $($(this).closest('.faq'));
        faq.toggleClass('open')
        faq.find('.faq__answer').slideToggle('fast');
    });

    $('.abc__button').on('click', function(e){
        e.preventDefault();
        $(this).closest('.abc').toggleClass('open');
    });

    $('.abc ul li').on('click', function(e){
        e.preventDefault();
        $('.abc').find('li').removeClass('active');
        $(this).addClass('active');
        let abc_value = $(this).attr('data-value');
        $('.abc__button span').text(abc_value);
        $('.abc__input').val(abc_value);
        $('.abc').removeClass('open');
    });
   
    // Result Slider
    if (isInformers) {

        let informers = new Swiper('.informers-slider',{ 
            loop: false,
            init: true,
            slidesPerView: 'auto',
            spaceBetween: 10,
            scrollbar: {
                el: '.informers__scrollbar-one',
            },
            breakpoints: {
                1024: {
                  slidesPerView: 3,
                  spaceBetween: 30,
                },
                1170: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                }
            }
        })

        informers.on('slideChange', function () {
            let sld = informers.realIndex + 1;
            $('.informers__counter--current').text(sld);
        });
    
    }

    if (isInformersTwo) {

        let informersTwo = new Swiper('.informers-slider-two',{ 
            loop: false,
            init: true,
            slidesPerView: 'auto',
            spaceBetween: 10,
            scrollbar: {
                el: '.informers__scrollbar-two',
            },
            breakpoints: {
                1024: {
                  slidesPerView: 3,
                  spaceBetween: 30,
                },
                1170: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                }
            }
        })

        informersTwo.on('slideChange', function () {
            let sld = informersTwo.realIndex + 1;
            $('.informers__counter--current-two').text(sld);
        });
    
    }

    if (isSportType) {

        let SportType = new Swiper('.sport-type-slider',{ 
            loop: false,
            init: true,
            simulateTouch: false,
            slidesPerView: 3,
            slidesPerColumn: 2,
            spaceBetween: 10,
            scrollbar: {
                el: '.sport-type__scrollbar',
            },
            breakpoints: {
                575: {
                    slidesPerView: 4,
                    slidesPerColumn: 2,
                    spaceBetween: 10,
                  },
                768: {
                    slidesPerView: 5,
                    slidesPerColumn: 2,
                    spaceBetween: 10,
                },
                992: {
                    slidesPerView: 9,
                    slidesPerColumn: 2,
                    spaceBetween: 0,
                },
                1170: {
                    slidesPerView: 9,
                    slidesPerColumn: 2,
                    spaceBetween: 10,
                }
            }
        })

        SportType.on('slideChange', function () {
            let sld = SportType.realIndex + 1;
            $('.sport-type__counter--current').text(sld);
        });
    }

    let extra = new Swiper('.extra-slider',{ 
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 20,
        breakpoints: {
            768: {
              slidesPerView: 2,
              spaceBetween: 30,
            }
        }
    })

    $('.event-filter__header').on('click', function(e){
        e.preventDefault();
        $('.event-filter').toggleClass('open');
    });


   let partitions_main = new Swiper('.partitions__main',{ 
        loop: false,
        init: true,
        longSwipes: false,
        shortSwipes: false,
        simulateTouch: false,
        slidesPerView: 1,
        spaceBetween: 30,
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
    });
    
    $('.partitions__nav--item').on('click', function(e){
        e.preventDefault();
        $(this).closest('.partitions__nav').find('.partitions__nav--item').removeClass('active');
        $(this).addClass('active');
        let tab = $(this).attr('data-nav');
        partitions_main.slideTo(tab, 600);
    });
    
    let isTournaments = $('div').is('.tournaments'); 

    if (isTournaments) {

        let tournaments_mobile_1 = new Swiper('.tournaments-mobile-1',{ 
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 20,
            scrollbar: {
                el: '.tournaments-scrollbar-1',
            },
            breakpoints: {
                768: {
                slidesPerView: 2,
                spaceBetween: 20,
                }
            }
        })

        tournaments_mobile_1.on('slideChange', function () {
            let sld = tournaments_mobile_1.realIndex + 1;
            $('.tournaments-counter-1').text(sld);
        });
        
        let tournaments_mobile_2 = new Swiper('.tournaments-mobile-2',{ 
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 20,
            scrollbar: {
                el: '.tournaments-scrollbar-2',
            },
            breakpoints: {
                768: {
                slidesPerView: 2,
                spaceBetween: 20,
                }
            }
        })

        tournaments_mobile_2.on('slideChange', function () {
            let sld = tournaments_mobile_2.realIndex + 1;
            $('.tournaments-counter-2').text(sld);
        });

                
        let tournaments_mobile_3 = new Swiper('.tournaments-mobile-3',{ 
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 20,
            scrollbar: {
                el: '.tournaments-scrollbar-3',
            },
            breakpoints: {
                768: {
                slidesPerView: 2,
                spaceBetween: 20,
                }
            }
        })

        tournaments_mobile_3.on('slideChange', function () {
            let sld = tournaments_mobile_3.realIndex + 1;
            $('.tournaments-counter-3').text(sld);
        });

                
        let tournaments_mobile_4 = new Swiper('.tournaments-mobile-4',{ 
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 20,
            scrollbar: {
                el: '.tournaments-scrollbar-4',
            },
            breakpoints: {
                768: {
                slidesPerView: 2,
                spaceBetween: 20,
                }
            }
        })

        tournaments_mobile_4.on('slideChange', function () {
            let sld = tournaments_mobile_4.realIndex + 1;
            $('.tournaments-counter-4').text(sld);
        });

    }

    $('.btn-modal').fancybox({
        autoFocus: false,
    });

    // Calendar Modal
    $('.date-switch__button').on('click', function(e){
        e.preventDefault();
        $(this).closest('.date-switch').toggleClass('open');
    });

    // Hide dropdown
    $('body').on('click', function (event) {    

        if ($(event.target).closest(".abc").length === 0) {
           $(".abc").removeClass('open');
        }      
    });

    $('.events-modal__scroll').mCustomScrollbar({
        theme: 'minimal-dark'
    });

    // Password field
    $('.field-icon-password').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
    });

    // lineups block
    $('.lineups__header--item').on('click', function(e){
        e.preventDefault();
        let $tab = $($(this).attr('data-tab'));
        $('.lineups__header--item').removeClass('active');
        $(this).toggleClass('active');
        $('.lineups__command').removeClass('active');
        $tab.addClass('active');
    });

     // Gallery Slider
    if (isMobileGallery) {
        let mobileGllery = new Swiper('.mobile-gallery-slider',{ 
            loop: false,
            init: true,
            initialSlide: 0,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.mobile-gallery-scrollbar',
            }
        })
    
        mobileGllery.on('slideChange', function () {
            let sldmob = mobileGllery.realIndex + 1;
            $('.mobile-gallery-active').text(sldmob);
        })
    }
        
});







    







  
    
      


